import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Person } from '../models/person.model';

@Injectable()
export class DataService {

  private personSource = new BehaviorSubject(new Person("", ""));
  currentPerson = this.personSource.asObservable();

  constructor() { }

  changePerson(person: Person) {
    this.personSource.next(person)
  }

  resetPerson(){
    this.personSource.next(new Person("", ""))
  }

}