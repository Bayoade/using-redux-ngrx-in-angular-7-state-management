import {Action} from '@ngrx/store';
import {Person} from '../models/person.model';
 
// 1. defining type of actions
export const ADD_PERSON = '[PERSON] Add';
export const REMOVE_PERSON = '[PERSON] Remove';
export const UPDATE_PERSON = '[PERSON] Update';
 
// 2. The AddPerson class
export class AddPerson implements Action {
  readonly type = ADD_PERSON;
  constructor(public payload: Person) {}
}
// 3. The RemovePerson class
export class RemovePerson implements Action {
  readonly type = REMOVE_PERSON;
  constructor(public payload: Person) {}
}

export class UpdatePerson implements Action{
   readonly type = UPDATE_PERSON
   constructor(public payload: Person) {}
}
 
// 4. export all action classes so that it can be used
// by reducers
export type Actions = AddPerson | RemovePerson | UpdatePerson;