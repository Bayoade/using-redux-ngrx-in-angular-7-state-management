import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Observable} from 'rxjs';
import { Store } from "@ngrx/store";
import { Person } from "./../../models/person.model";
import { AppState } from "./../../states/state";
import * as PersonActions from '../../actions/person.actions';
import { WritePersonComponent } from '../write-person/write-person.component';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: "app-read-person",
  templateUrl: "./read-person.component.html",
  styleUrls: ['./read-person.component.sass']
})
export class ReadPersonComponent implements OnInit {
  persons: Observable<Person[]>;
  @Output() personEvent = new EventEmitter<Person>();

  constructor(private store: Store<AppState>, private _dataService: DataService) {
    this.persons = store.select('person');
  }

  deletePerson(p: Person) {
    this.store.dispatch(new PersonActions.RemovePerson(p));
  }

  editPerson(p: Person){
    this._dataService.changePerson(p);
  }

  updatePerson(p: Person){
    this.store.dispatch(new PersonActions.UpdatePerson(p));
  }
  ngOnInit() {}
}
