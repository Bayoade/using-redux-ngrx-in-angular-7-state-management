import { Store } from '@ngrx/store';
import { Component, OnInit, Input } from '@angular/core';
import { AppState } from '../../states/state';
import { Person } from '../../models/person.model';
import * as PersonActions from '../../actions/person.actions';
import {Observable} from 'rxjs';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-write-person',
  templateUrl: './write-person.component.html',
  styleUrls: ['./write-person.component.sass']
})
export class WritePersonComponent implements OnInit {
  person: Person;
  // 2. Inject Store for AppState in Constructor
  constructor(private store: Store<AppState>, private _dateService: DataService) {
    this._dateService.currentPerson.subscribe(p => this.person = p);
  }
 
  ngOnInit() {
  }
 
  // 3. dispatch the AddPerson Action
  addPerson() {
    if(this.person.Id){
      this.store.dispatch(new PersonActions.UpdatePerson(this.person));
      return this.clearPerson();
    }
    this.person.Id = this.generateRandomNumber(1, 10) + this.person.Name;
    this.store.dispatch(
       new PersonActions.AddPerson(this.person)
    );
    this.clearPerson();
  }
  clearPerson() {
    this._dateService.resetPerson();
  }

  generateRandomNumber(ourMin: number, ourMax: number) {
    return Math.floor(Math.random() * (ourMax - ourMin + 1)) + ourMin;
  }
}
