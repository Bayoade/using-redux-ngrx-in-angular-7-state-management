// 1a. import reducer
import { reducer } from './reducers/person.reducer';
// 1b. import StoreModule
import { StoreModule } from '@ngrx/store';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReadPersonComponent } from './components/read-person/read-person.component';
import { WritePersonComponent } from './components/write-person/write-person.component';
import { DataService } from './service/data.service';

@NgModule({
  declarations: [
    AppComponent,
    ReadPersonComponent,
    WritePersonComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    StoreModule.forRoot({
      person : reducer
  })
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
